/**
 * @author Zacharie Makeen
 */
package backend;
import java.util.Random;

public class RpsGame {

	private int wins = 0;
	private int ties = 0;
	private int loses = 0;
	private Random randomInteger = new Random();
	public int getWins() {

		return this.wins;
	}
	public int getTies() {

		return this.ties;
	}
	public int getLoses() {

		return this.loses;
	}
	public String playRound(String choice) {

		int playerChoice = choiceToInt(choice);
		int computerChoice = randomInteger.nextInt(3);
		String computerPlay = choiceToString(computerChoice);
		int result = determineWinner(playerChoice, computerChoice);
		switch(result) {

			case -1: 
				this.loses++;
				return ("Computer played " + computerPlay + " and computer won!");
			case 0: 
				this.ties++;
				return ("Computer played " + computerPlay + " and both players tied!");
			case 1: 
				this.wins++;
				return ("Computer played " + computerPlay + " and player won!");
		}
		return ("The round has failed to play!");
	}
	private int choiceToInt(String choice) {

		switch(choice) {

			case "rock": return 0;
			case "paper": return 1;
		}
		return 2;
	}
	private String choiceToString(int choice) {

		switch(choice) {

			case 0: return "rock";
			case 1: return "paper";
		}
		return "scissors";
	}
	private int determineWinner(int playerChoice, int computerChoice) {

		if(playerChoice == computerChoice) {

			return 0;
		} else if((playerChoice == 0 && computerChoice == 2) || (playerChoice == 2 && computerChoice == 1) || (playerChoice == 1 && computerChoice == 0)) {

			return 1;
		} else {

			return -1;
		}
	}
}

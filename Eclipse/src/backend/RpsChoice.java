/**
 * @author Zacharie Makeen
 */
package backend;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.TextField;

public class RpsChoice implements EventHandler<ActionEvent> {

	private TextField message;
	private TextField wins;
	private TextField loses;
	private TextField ties;
	private String choice;
	private RpsGame game;
	public RpsChoice(TextField message, TextField wins, TextField loses, TextField ties, String choice, RpsGame game) {

		this.message = message;
		this.wins = wins;
		this.loses = loses;
		this.ties = ties;
		this.choice = choice;
		this.game = game;
	}
	@Override
	public void handle(ActionEvent e) {

		String result = game.playRound(choice);
		this.message.setText(result);
		String winsMessage = String.valueOf(game.getWins());
		String losesMessage = String.valueOf(game.getLoses());
		String tiesMessage = String.valueOf(game.getTies());
		this.wins.setText("Wins: " + winsMessage);
		this.loses.setText("Loses: " + losesMessage);
		this.ties.setText("Ties: " + tiesMessage);
	}
}

/**
 * @author Zacharie Makeen
 */
package frontend;
import backend.*;
import javafx.application.*;
import javafx.stage.*;
import javafx.scene.*;
import javafx.scene.paint.*;
import javafx.scene.control.*;
import javafx.scene.layout.*;

public class RpsApplication extends Application {
	private RpsGame game = new RpsGame();
	public void start(Stage stage) {
		Group root = new Group();

		Button rock = new Button("Rock");
		Button paper = new Button("Paper");
		Button scissors = new Button("Scissors");

		TextField message = new TextField("Welcome");
		TextField winsMessage = new TextField("Wins: " + game.getWins());
		TextField losesMessage= new TextField("Loses: " + game.getLoses());
		TextField tiesMessage = new TextField("Ties: " + game.getTies());
		message.setPrefWidth(300);
		winsMessage.setPrefWidth(80);
		losesMessage.setPrefWidth(80);
		tiesMessage.setPrefWidth(80);

		RpsChoice rockChoice = new RpsChoice(message, winsMessage, losesMessage, tiesMessage, "rock", game);
		RpsChoice paperChoice = new RpsChoice(message, winsMessage, losesMessage, tiesMessage, "paper", game);
		RpsChoice scissorsChoice = new RpsChoice(message, winsMessage, losesMessage, tiesMessage, "scissors", game);
		rock.setOnAction(rockChoice);
		paper.setOnAction(paperChoice);
		scissors.setOnAction(scissorsChoice);

		HBox buttons = new HBox();
		buttons.getChildren().addAll(rock, paper, scissors);
		HBox textFields = new HBox();
		textFields.getChildren().addAll(message, winsMessage, losesMessage, tiesMessage);
		VBox overall = new VBox();
		overall.getChildren().addAll(buttons, textFields);
		root.getChildren().add(overall);

		Scene scene = new Scene(root, 650, 300); 
		scene.setFill(Color.BLACK);

		stage.setTitle("Rock Paper Scissors"); 
		stage.setScene(scene); 
		stage.show(); 
	}
    public static void main(String[] args) {
        Application.launch(args);
    }
}